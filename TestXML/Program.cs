﻿using System;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;

public class Program
{
    public static void Main1()
    {
        var students = new List<Student> {
            new Student { StudentID = 1, StudentName = "Bill", Age = 20, Address = "New York" },
            new Student { StudentID = 2, StudentName = "Tom", Age = 22, Address = "London" },
            new Student { StudentID = 3, StudentName = "Jane", Age = 21, Address = "Paris" }
        };

        using (var stream = new FileStream("students.xml", FileMode.Create))
        {
            var serializer = new XmlSerializer(typeof(List<Student>));
            serializer.Serialize(stream, students);
        }
    }
}

public class Student
{
    public int StudentID { get; set; }
    public string StudentName { get; set; }
    public int Age { get; set; }
    public string Address { get; set; }
}

