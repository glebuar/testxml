﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestXML
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Linq;

    public class StudentsBook
    {
        public static void Main()
        {
            string filePath = @"books.xml";

            try
            {
                // Read the XML document
                XDocument xmlDoc = XDocument.Load(filePath);

                // Get the root element
                XElement library = xmlDoc.Element("library");

                // Create a list of students
                List<Student> students = new List<Student>()
            {
                new Student() { StudentID = 1, StudentName = "Bill", Age = 20, Address = "New York", BooksTaken = "Harry Potter" },
                new Student() { StudentID = 2, StudentName = "Tom", Age = 22, Address = "London", BooksTaken = "Learning XML" },
                new Student() { StudentID = 3, StudentName = "Jane", Age = 21, Address = "Paris", BooksTaken = "Everyday Italian" }
            };

                // Loop through the books and add the students who took the book
                foreach (XElement book in library.Elements("book"))
                {
                    string title = book.Element("title").Value;

                    // Find the student who took the book
                    Student student = students.Find(s => s.BooksTaken == title);

                    if (student != null)
                    {
                        // Add the student to the book element
                        book.Add(new XElement("student",
                                        new XElement("id", student.StudentID),
                                        new XElement("name", student.StudentName),
                                        new XElement("address", student.Address)));
                    }
                }

                // Save the updated XML document
                using (FileStream stream = new FileStream(filePath, FileMode.Create))
                {
                    xmlDoc.Save(stream);
                }

                Console.WriteLine("Books.xml file updated successfully.");
            }
            catch (IOException ex)
            {
                // Log the error to a file
                using (StreamWriter writer = new StreamWriter("error.log"))
                {
                    writer.WriteLine(DateTime.Now.ToString() + " - An I/O error occurred: " + ex.Message);
                }

                Console.WriteLine("An I/O error occurred while processing the file.");
            }
            catch (Exception ex)
            {
                // Log the error to a file
                using (StreamWriter writer = new StreamWriter("error.log"))
                {
                    writer.WriteLine(DateTime.Now.ToString() + " - An error occurred: " + ex.Message);
                }

                Console.WriteLine("An error occurred while processing the file.");
            }
        }
    }

    public class Student
    {
        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public string BooksTaken { get; set; }
    }

}
